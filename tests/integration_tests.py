import json
from urllib.parse import urlencode, urljoin, urlparse

import pytest

from ..main import make_app
from ..settings import Settings


@pytest.fixture
def app():
    app_settings: Settings = Settings(_env_file=".env-test")
    app = make_app(app_settings)
    return app


@pytest.fixture
def shorten_url_request(http_client, base_url):
    def _requester(url: str):
        return http_client.fetch(
            request=urljoin(base_url, "shorten/"),
            method="POST",
            body=urlencode({"url": url}),
            raise_error=False,
        )

    return _requester


@pytest.mark.gen_test
async def test_base_url(http_client, base_url):
    response = await http_client.fetch(base_url)
    assert response.code == 200


@pytest.mark.gen_test
async def test_getting_shortened_url(shorten_url_request):
    response = await shorten_url_request("https://google.com")
    assert response.code == 200
    request_body = json.loads(response.body)
    assert "short_url" in request_body, "shortened_url should appear in response body"
    assert request_body["short_url"], "Short URL shouldn't be empty"
    assert request_body["short_url"].startswith(("http://", "https://")), "Returned url should start from http or https"


@pytest.mark.gen_test
async def test_empty_url(shorten_url_request):
    response = await shorten_url_request("")
    assert response.code == 400


@pytest.mark.gen_test
async def test_posting_same_url_twice(shorten_url_request):
    first_response = await shorten_url_request("https://google.com")
    assert first_response.code == 200
    first_response_body = json.loads(first_response.body)

    second_response = await shorten_url_request("https://google.com")
    assert second_response.code == 200
    second_response_body = json.loads(second_response.body)

    assert not (
        first_response_body["short_url"] == second_response_body["short_url"]
    ), "Returned URL should be different"


@pytest.mark.gen_test
async def test_redirection(shorten_url_request, http_client, base_url, http_port):
    long_url = "https://google.com"
    response = await shorten_url_request(long_url)
    response_body = json.loads(response.body)
    short_url = response_body["short_url"]
    redirection_url = urlparse(short_url)
    redirection_url = redirection_url._replace(netloc=base_url.replace("http://", "")).geturl()

    redirect_response = await http_client.fetch(redirection_url, max_redirects=0, raise_error=False)
    assert redirect_response.code == 302
    assert redirect_response.headers["Location"] == long_url


@pytest.mark.gen_test
async def test_redirection_for_invalid_url(shorten_url_request, http_client, base_url, http_port):
    url = urljoin(base_url, "S0meTest")
    response = await http_client.fetch(url, raise_error=False)
    assert response.code == 404
