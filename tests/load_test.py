import asyncio
import json
from urllib.parse import urlencode

from tornado.httpclient import AsyncHTTPClient


async def f():
    http_client = AsyncHTTPClient()
    try:
        response = await http_client.fetch(
            "http://localhost/shorten/",
            method="POST",
            body=urlencode({"url": "https://google.com"}),
        )
    except Exception as e:
        print("Error: %s" % e)
    else:
        return json.loads(response.body)


async def load_test():
    list_of_tasks = [f() for _ in range(3000)]
    results = await asyncio.gather(*list_of_tasks)
    print(results)


if __name__ == "__main__":
    asyncio.run(load_test())
