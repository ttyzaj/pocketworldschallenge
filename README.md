# PocketWorlds assignment

## Web service for URL shortening

This projects contains web service that allows users to send a long URL and receive a shortened one.
Also when user enters the shortened URL webservice redirects to the long URL.

## Usage
```shell 
docker-compose up -d
``` 

builds the image and runs the server 

Default setting uses the port 80 and http://localhost .
It can be changed in `docker-compose.yml`

Main page serves simple form allowing user to enter the URL. 
Clicking "Shorten URL" performs a call to http://localhost/shorten/ endpoint,
that returns json output e.g.


`{"short_url": "http://localhost/ZbKoLAc7"}`

Entering such URL should redirect to the URL provided in form field.

It's also possible to call http://localhost/shorten/ endpoint directly and provide url in `url` body param e.g.

```shell
curl -X POST -F 'url=https://google.com' http://localhost/shorten/
```


## Technologies used

Web service written in Python 3 using the libraries listed in requirements.txt

### Tornado web server
Tornado is a Python web framework and asynchronous networking library. 
By using non-blocking network I/O, Tornado can scale to tens of thousands of open connections.
It fully integrates asyncio library and as of Tornado 6.0, `IOLoop` is a wrapper around the `asyncio` event
    loop. In general, libraries designed for use with asyncio can be mixed freely with Tornado. 

### Redis as database
I chose Redis as a database because of O(1) time complexity of `get` and `setnx` 
and it was all what was needed in this case. I enabled persistent storage using AOF setting.
For this project it looked enough. In production environment I could enable also RDB 
or chose another Key-Value database, depending on scale and other requirements.

### Settings (pydantic)
Settings are validated using `pydantic` library. They can be passed through environment variables 
(e.g. during deployment) or .env file (I added .env file to repository, although usually there is no need)

### Metrics (Prometheus)
Metrics are collected by `tornado-prometheus` library and can be accessed at `/metrics` endpoint. 
http://localhost/metrics

I think that it should be possible to connect Grafana to it and create nice dashboard out of it

### Logging
Python logging module has been used. Providing some detailed logs from the code exection. 
Basic logs are also available in Tornado's web server logs. 

## Running the tests locally

Tests can be run either using local Redis or using dockerized one. 

You can specify it in `.env-test` file in `tests` directory

### Integration tests 
To run integration tests either start local Redis (if you use one) 
or start dockerized redis and provide URL to `.env-test` file or just `docker-compose up -d`

- Enter virtual environment (with requirements installed)
- Enter `tests` directory
- Run:
```shell 
PYTHONPATH=$(dirname `pwd`) pytest integration_tests.py
``` 

### Loadtest
There is also simple load test calling `/shorten/` endpoint 3000 times.
You can run it by
```shell
python loadtest.py
```

It requires server to be running so run `docker-compose up -d` before :) 