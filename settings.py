from pydantic import BaseSettings


class Settings(BaseSettings):
    redis_url: str
    application_url: str
    port: int
