import json
import logging
import random
import string
from urllib.parse import urlparse

import aioredis
import tornado.ioloop
import tornado.web
from tornado_prometheus import MetricsHandler, PrometheusMixIn

from settings import Settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class UrlShorteningApp(PrometheusMixIn, tornado.web.Application):
    pass


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("templates/main.html", title="PocketWorlds Challenge URL Shortener")


class RedirectToLongUrl(tornado.web.RequestHandler):
    async def get(self, url_identifier):
        long_url = await self.settings["redis"].get(url_identifier)
        if not long_url:
            logging.error(f"Short URL id {url_identifier} not found in Redis")
            self.set_status(404, "URL not found.")
            return self.render("templates/404.html", title=f"URL {url_identifier} not found", url_id=url_identifier)
        return self.redirect(long_url)


class UrlShortenHandler(tornado.web.RequestHandler):
    async def post(self):
        url = self.get_body_argument("url")
        logging.debug(f"Received {url} body param")
        if not self._is_valid_url(url):
            logging.debug(f"{url} identified as invalid URL")
            return self.send_error(400, reason="Invalid URL")
        key_added = False
        while not key_added:
            # non-elegant way of handling very rare case where we get short url that already exists
            short_url_id = self._get_short_url_id()
            key_added = await self.settings["redis"].setnx(short_url_id, url)
            if key_added:
                logging.debug(f"{short_url_id} id pointing to {url} added to Redis")
            else:
                logging.debug(f"{short_url_id} already in redis, generating new short url id")
        return self.write(json.dumps({"short_url": f"{self.settings.get('app_url')}/{short_url_id}"}))

    def _is_valid_url(self, url: string) -> bool:
        try:
            address = urlparse(url)
            if all([address.scheme, address.netloc]):
                return True
            else:
                return False
        except ValueError:
            return False

    def _get_short_url_id(self) -> str:
        return "".join(random.choices(string.ascii_letters + string.digits, k=8))


def make_app(settings: Settings) -> UrlShorteningApp:
    redis = aioredis.from_url(settings.redis_url, decode_responses=True)
    return UrlShorteningApp(
        [
            (r"/", MainHandler),
            (r"/shorten/.*", UrlShortenHandler),
            (r"/metrics", MetricsHandler),
            (r"/(.*)", RedirectToLongUrl),
        ],
        debug=True,
        redis=redis,
        app_url=settings.application_url,
    )


if __name__ == "__main__":
    app_settings: Settings = Settings(_env_file=".env")
    app: UrlShorteningApp = make_app(app_settings)
    app.listen(app_settings.port)
    tornado.ioloop.IOLoop.current().start()
